import { make, isUrl } from './helpers';
import galleryClient from './galleryClient';

/**
 * Renders control panel view
 */
export default class ControlPanel {
  /**
   * @param {{ api: object, config: object, cssClasses: object,
   *  onSelectImage: Function, readOnly: Boolean }}
   *  api - Editorjs API
   *  config - Tool custom config
   *  readOnly - read-only mode flag
   *  cssClasses - Css class names
   *  onSelectImage - Image selection callback
   */
  constructor({
    api, config, cssClasses, onSelectImage, readOnly,
  }) {
    this.api = api;
    this.config = config;
    this.readOnly = readOnly;

    this.cssClasses = {
      ...cssClasses,
      controlPanel: 'inline-image__control-panel',
      tabWrapper: 'inline-image__tab-wrapper',
      tab: 'inline-image__tab',
      embedButton: 'inline-image__embed-button',
      search: 'inline-image__search',
      imageGallery: 'inline-image__image-gallery',
      noResults: 'inline-image__no-results',
      imgWrapper: 'inline-image__img-wrapper',
      thumb: 'inline-image__thumb',
      active: 'active',
      hidden: 'panel-hidden',
      scroll: 'panel-scroll',
    };

    this.onSelectImage = onSelectImage;

    this.nodes = {
      loader: null,
      embedUrlTab: null,
      galleryTab: null,
      embedUrlPanel: null,
      galleryPanel: null,
      imageGallery: null,
      searchInput: null,
    };
    this.showEmbedTab = this.config.embed ? this.config.embed.display : true;
    this.galleryClient = new galleryClient(this.config);
  }

  /**
   * Creates Control Panel components
   *
   * @returns {HTMLDivElement}
   */
  render() {
    const wrapper = make('div', this.cssClasses.controlPanel);
    const tabWrapper = make('div', this.cssClasses.tabWrapper);
    const embedUrlTab = make('div', [this.cssClasses.tab, this.cssClasses.active], {
      innerHTML: 'URL',
      onclick: () => this.showEmbedUrlPanel(),
    });
    const galleryTab = make('div', [this.cssClasses.tab, this.showEmbedTab ? null : this.cssClasses.active], {
      innerHTML: 'Fichier du projet',
      onclick: () => this.showGalleryPanel(),
    });

    const embedUrlPanel = this.renderEmbedUrlPanel();
    const galleryPanel = this.renderGalleryPanel();

    if (this.showEmbedTab) { tabWrapper.appendChild(embedUrlTab); }
    tabWrapper.appendChild(galleryTab);
    wrapper.appendChild(tabWrapper);
    if (this.showEmbedTab) { wrapper.appendChild(embedUrlPanel); }
    wrapper.appendChild(galleryPanel);

    this.nodes.embedUrlPanel = this.showEmbedTab ? embedUrlPanel : null;
    this.nodes.galleryPanel = galleryPanel;
    this.nodes.embedUrlTab = this.showEmbedTab ? embedUrlTab : null;
    this.nodes.galleryTab = galleryTab;

    return wrapper;
  }

  /**
   * Shows "Embed Url" control panel
   *
   * @returns {void}
   */
  showEmbedUrlPanel() {
    this.nodes.embedUrlTab.classList.add(this.cssClasses.active);
    this.nodes.galleryTab.classList.remove(this.cssClasses.active);
    this.nodes.embedUrlPanel.classList.remove(this.cssClasses.hidden);
    this.nodes.galleryPanel.classList.add(this.cssClasses.hidden);
  }

  /**
   * Shows Gallery control panel
   *
   * @returns {void}
   */
  showGalleryPanel() {
    this.nodes.galleryTab.classList.add(this.cssClasses.active);
    this.nodes.embedUrlTab.classList.remove(this.cssClasses.active);
    this.nodes.galleryPanel.classList.remove(this.cssClasses.hidden);
    this.nodes.embedUrlPanel.classList.add(this.cssClasses.hidden);
    this.searchInputHandler();
  }

  /**
   * Creates "Embed Url" control panel
   *
   * @returns {HTMLDivElement}
   */
  renderEmbedUrlPanel() {
    const wrapper = make('div');
    const urlInput = make('div', [this.cssClasses.input, this.cssClasses.caption], {
      id: 'image-url',
      contentEditable: !this.readOnly,
    });
    const embedImageButton = make('div', [this.cssClasses.embedButton, this.cssClasses.input], {
      id: 'embed-button',
      disabled: true,
      innerHTML: 'Ajouter l\'image',
      // onclick: () => this.embedButtonClicked(urlInput.innerHTML),
    });

    urlInput.dataset.placeholder = 'Entrer l\'URL de l\'image...';

    wrapper.appendChild(urlInput);
    wrapper.appendChild(embedImageButton);


    return wrapper;
  }

  /**
   * OnClick handler for Embed Image Button
   *
   * @param {string} imageUrl embedded image url
   * @returns {void}
   */
  embedButtonClicked(imageUrl) {
    if (isUrl(imageUrl)) {
      this.onSelectImage({ url: imageUrl });
    } else {
      this.api.notifier.show({
        message: 'Please enter a valid url.',
        style: 'error',
      });
    }
  }

  /**
   * Creates gallery control panel
   *
   * @returns {HTMLDivElement}
   */
  renderGalleryPanel() {
    const wrapper = make('div', this.showEmbedTab ? this.cssClasses.hidden : null);
    const imageGallery = make('div', this.cssClasses.imageGallery);

    wrapper.appendChild(imageGallery);

    this.nodes.imageGallery = imageGallery;

    return wrapper;
  }

  /**
   * OnInput handler for Search input
   *
   * @returns {void}
   */
  searchInputHandler() {
    this.showLoader();
    this.performSearch();
  }

  /**
   * Shows a loader spinner on image gallery
   *
   * @returns {void}
   */
  showLoader() {
    this.nodes.imageGallery.innerHTML = '';
    this.nodes.loader = make('div', this.cssClasses.loading);
    this.nodes.imageGallery.appendChild(this.nodes.loader);
  }

    /**
   * Performs image search on user input.
   * Defines a timeout for preventing multiple requests
   *
   * @returns {void}
   */
    performSearch() {
      clearTimeout(this.searchTimeout);
      this.searchTimeout = setTimeout(() => {
        // const query = this.nodes.searchInput.innerHTML;
        this.galleryClient.searchImages((results) => {
            this.appendImagesToGallery(results)
        });
  
  
      }, 1000);
    }

  /**
   * Creates the image gallery from images stored in the indexedDB
   *
   * @param {Array} results Images stored in the indexedDB
   */
  appendImagesToGallery(results) {
    this.nodes.imageGallery.innerHTML = '';
    if (results && results.length) {
      this.nodes.galleryPanel.classList.add(this.cssClasses.scroll);
      results.forEach((image) => {
        this.createThumbImage(image);
      });
    } else {
      const noResults = make('div', this.cssClasses.noResults, {
        innerHTML: 'Pas d\'images dans le projet. Veuillez en ajouter en cliquant sur le bouton <span class="fas fa-file-circle-plus"></span>',
      });
      this.nodes.imageGallery.appendChild(noResults);
      this.nodes.galleryPanel.classList.remove(this.cssClasses.scroll);
    }
  }

  /**
   * Creates a thumb image and appends it to the image gallery
   *
   * @param {Object} image Image object
   * @returns {void}
   */
  createThumbImage(image) {
    const imgWrapper = make('div', this.cssClasses.imgWrapper);
    const img = make('img', this.cssClasses.thumb, {
      src: image.thumb,
      onclick: () => this.downloadGalleryImage(image),
    });

    const imageCredits = make('div', this.cssClasses.imageCredits, {
      innerHTML: image.name,
    });

    imgWrapper.appendChild(img);
    imgWrapper.appendChild(imageCredits);
    this.nodes.imageGallery.append(imgWrapper);
  }


    /**
   * Handler for embedding gallery image
   */
    downloadGalleryImage({
      url, name, downloadLocation,
    }) {
      this.onSelectImage({
        url,
        name,
      });
    }
}
