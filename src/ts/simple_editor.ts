import EditorJS from '@editorjs/editorjs'
// @ts-ignore
import Header from '@editorjs/header';
// @ts-ignore
import List from "@editorjs/list";
// @ts-ignore
import Table from '@editorjs/table'
// @ts-ignore
import InlineImage from '../modules/editorjs-inline-image';
// @ts-ignore
import Undo from 'editorjs-undo';
// @ts-ignore
import Underline from '@editorjs/underline';
// @ts-ignore
import DragDrop from 'editorjs-drag-drop';

import JSONTeXTranslator from "./json_to_tex";
import Editor from "./editor";

export default class WYSIWYGEditor {
    private _simpleEditor: any;
    private _editor: any;
    private _translator: any;

    constructor() {
        this._simpleEditor = null;
    }

    public init() {
        return new Promise<void>((resolve, reject) => {

            // Init the simple editor
            this._simpleEditor = new EditorJS({
                tools: {
                    header: {
                        class: Header,
                        config: {
                            placeholder: 'Entrez un titre',
                            levels: [1, 2, 3],
                            defaultLevel: 1,
                        },
                    },
                    list: List,
                    table: Table,
                    image: {
                        class: InlineImage,
                        inlineToolbar: true,
                        config: {
                            embed: {
                                display: true,
                            },
                        }
                    },
                    underline: Underline,
                },

                /**
                 * Internationalzation config
                 */
                i18n: {
                    /**
                     * @type {I18nDictionary}
                     */
                    messages: {
                        ui: {
                            "blockTunes": {
                                "toggler": {
                                    "Click to tune": "Cliquer pour modifier",
                                    "or drag to move": "ou glisser pour déplacer"
                                },
                            },
                            "inlineToolbar": {
                                "converter": {
                                    "Convert to": "Convertir en"
                                }
                            },
                            "toolbar": {
                                "toolbox": {
                                    "Add": "Ajouter"
                                }
                            }
                        },
                        toolNames: {
                            "Text": "Тexte",
                            "Heading": "Section",
                            "List": "Liste",
                            "Warning": "Avertissement",
                            "Checklist": "Liste de contrôle",
                            "Quote": "Citation",
                            "Code": "Code",
                            "Delimiter": "Ligne de séparation",
                            "Raw HTML": "HTML brut",
                            "Table": "Tableau",
                            "Link": "Lien",
                            "Marker": "Surlignage",
                            "Bold": "Gras",
                            "Italic": "Italique",
                            "InlineImage": "Image",
                        },
                        tools: {
                            "warning": {
                                "Title": "Titre",
                                "Message": "Message"
                            },
                            "link": {
                                "Add a link": "Ajouter un lien"
                            },
                            "stub": {
                                'The block can not be displayed correctly.': 'Le bloc ne peut pas être affiché correctement.',
                            },
                        },
                        blockTunes: {
                            "delete": {
                                "Delete": "Supprimer",
                                "Click to delete": "Confirmer ?"
                            },
                            "moveUp": {
                                "Move up": "Monter"
                            },
                            "moveDown": {
                                "Move down": "Descendre"
                            },
                            "header": {
                                "Heading": "Section"
                            }

                        },
                    }
                },
                holder: 'WYSIWYGEditor',
                autofocus: true,
                placeholder: 'Saisissez votre texte ici...',
                onChange: this.changeHandler,
                onReady: () => {
                    new Undo({ editor: this._simpleEditor });
                    new DragDrop(this._simpleEditor);
                    console.log('WYSIWYGEditor initialized')
                    resolve()
                }
            });
            // Init the translator
            this._translator = new JSONTeXTranslator();
            // Init the TeX editor
            this._editor = new Editor("main.tex");
        })
    }

    public save() {
        return new Promise((resolve, reject) => {
            // Get the JSON from the editor
            this._simpleEditor.save().then((editorData: any) => {
                console.log(editorData)
                resolve(editorData)
            }).catch((error: any) => {
                reject(error)
            })
        })
    }

    public empty() {
        // Check if the editor is ready
        if (!this._simpleEditor.isReady) {
            console.log('Editor not ready')
            return
        }
        // Clear the editor
        this._simpleEditor.blocks.clear()
        // Add a new paragraph
        this._simpleEditor.blocks.insert("paragraph", {
            text: " "
        })
        // Clear the TeX editor
        this._editor.setValue("")
    }

    public setEditorContent(json: any) {
        // Check if the editor is ready
        if (!this._simpleEditor.isReady) {
            console.log('Editor not ready')
            return
        }
        // Set the JSON content in the simple editor
        this._simpleEditor.blocks.render(json);
        // If blocks is empty, add a new paragraph
        if (json.blocks.length == 0) {
            this._simpleEditor.blocks.insert("paragraph", {
                text: " "
            })
        }
    }

    private changeHandler = () => {
        // Get the JSON from the editor
        this._simpleEditor.save().then((editorData: any) => {
            //    Translate the JSON to TeX
            this._translator.translate(editorData).then((outputData: string) => {
                // Update the TeX editor
                this._editor.setValue(outputData)
                this.setHighlighting()

            }).catch((error: any) => {
                console.log('Translation failed: ', error)
            })
        })
    }

    private setHighlighting() {
        // Creates an event listener for current block in the simple editor. Highlights the corresponding TeX code when the block is clicked.
        let index = this._simpleEditor.blocks.getCurrentBlockIndex()
        let id = this._simpleEditor.blocks.getBlockByIndex(index).id;
        let blockTex = this._translator.getBlockTex(id);
        this._editor.highlightBlock(blockTex);
    }

}