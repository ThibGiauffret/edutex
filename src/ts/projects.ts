export default class Projects {

    public _projectName: string;
    public projectAlert: HTMLDivElement;
    public hideLoader: any;

    public constructor() {
        this._projectName = "";
        this.projectAlert = document.getElementById("projectAlert") as HTMLDivElement;
    }

    // Load the project names from the local storage and add them to the project list
    public load() {
        // Empty the project list
        let projectList = document.getElementById("projectList") as HTMLDivElement
        projectList.innerHTML = "";
        // Hide the loading spinner
        let loadingSpinner = document.getElementById("loadingSpinner") as HTMLDivElement;
        loadingSpinner.classList.add("d-none");
        // Add all the project names to the project list table
        let projects_string = localStorage.getItem("projectNames");
        let projects = JSON.parse(projects_string || "[]");
        // If there are projects, add them to the project list
        if (projects.length > 0) {
            projects.forEach((project: any) => {
                let tr = document.createElement("tr");
                tr.setAttribute("id", project.name);
                let td = document.createElement("td");
                td.innerHTML = '<span class="mx-2">' + project.name + '</span>';
                tr.appendChild(td);
                let td2 = document.createElement("td");

                let span = document.createElement("span");
                span.classList.add("float-end", "mx-1");
                let deleteButton = document.createElement("button");
                deleteButton.classList.add("btn", "btn-sm", "btn-danger", "projectDeleteBtn");
                deleteButton.setAttribute("data-project-name", project.name);
                let deleteIcon = document.createElement("i");
                deleteIcon.classList.add("fas", "fa-trash");
                deleteButton.appendChild(deleteIcon);
                span.appendChild(deleteButton);

                let openButton = document.createElement("button");
                openButton.classList.add("btn", "btn-sm", "btn-primary", "ms-2", "projectOpenBtn");
                openButton.setAttribute("data-project-name", project.name);
                let openIcon = document.createElement("i");
                openIcon.classList.add("fas", "fa-arrow-right");
                openButton.appendChild(openIcon);
                span.appendChild(openButton);


                td2.appendChild(span);
                tr.appendChild(td2);
                projectList.appendChild(tr);
            });
        } else {
            // Show the projectPlaceholder
            let tr = document.createElement("tr");
            tr.setAttribute("id", "projectPlaceholder");
            let td = document.createElement("td");
            td.setAttribute("colspan", "2");
            td.classList.add("text-center");
            td.innerHTML = '<span>Aucun projet trouvé.</span>';
            tr.appendChild(td);

            projectList.appendChild(tr);
        }
        
        // Show the project list
        let projectSelector = document.getElementById("projectSelector") as HTMLDivElement;
        projectSelector.classList.remove("d-none");


    }

    // Create a new project
    public add(workspace: string = "default") {
        let projectInput = document.getElementById("projectName") as HTMLInputElement;
        let projectAlert = document.getElementById("projectAlert") as HTMLDivElement;
        let project = projectInput.value;
        // Test if the project name is empty
        if (project == "" || project == "error") {
            projectAlert.classList.remove("d-none");
            projectAlert.innerHTML = "<i class='fa-solid fa-fw fa-exclamation-triangle'></i>&nbsp;Veuillez entrer un nom de projet.";
            return "error"; 
        }
        // Replace all spaces with underscores
        project = project.replace(/\s/g, "_");
        // Test if the project name already exists
        let projects_string = localStorage.getItem("projectNames");
        let projects = JSON.parse(projects_string || "[]");
        if (projects.length > 0) {
            if (projects.includes(project)) {
                projectAlert.classList.remove("d-none");
                projectAlert.innerHTML = "<i class='fa-solid fa-fw fa-exclamation-triangle'></i>&nbsp;Un projet avec ce nom existe déjà.";
                return 'error';
            }
        }
        // Empty the project name input
        projectInput.value = "";
        this._projectName = project;
        // Store the project name in the local storage
        this.storeName(project, workspace);
        this.open(project);
        return project;
    }

    // Store the project name in the local storage
    private storeName(project: string, workspace: string = "default") {
        // Get the project names from the local storage
        let projects_string = localStorage.getItem("projectNames");
        let projects = JSON.parse(projects_string || "[]");
        console.log(projects)
        // Add a key-value pair to the projects object
        projects.push({name: project, workspace: workspace});
        // Store the projectNames array in the local storage
        localStorage.setItem("projectNames", JSON.stringify(projects));
    }

    // Open a project
    public open(project: string) {
        this._projectName = project;
        let projectNameSpan = document.getElementById("projectNameSpan") as HTMLSpanElement;
        projectNameSpan.innerHTML = "Projet : " + project;
        // Set the current project name in the local storage
        localStorage.setItem("currentProject", project);
    }

    // Show the project selector (loader)
    public show() {
        // Hide the project alert
        let projectAlert = document.getElementById("projectAlert") as HTMLDivElement;
        projectAlert.classList.add("d-none");
        // Load the projects
        this.load();
    }
}


