import ace from "ace-builds";
import "ace-builds/webpack-resolver";
import "ace-builds/src-noconflict/ext-language_tools";
import "ace-builds/src-noconflict/ext-beautify";
import { keywords } from "./keywords";

export default class _editor {
  private _editor: any;
  private _name: string;

  public constructor(name: string) {
    this._name = name;
    this.addToDOM(name);
    this._editor = ace.edit(name);
  }

  // Initialize the Ace _editor.
  public async init() {

    // Set the theme and the mode
    this._editor.setTheme("ace/theme/monokai");
    this._editor.session.setMode("ace/mode/latex");

    // Set the options
    this._editor.setOptions({
      enableBasicAutocompletion: true,
      enableSnippets: true,
      enableLiveAutocompletion: true,
    });
    this._editor.session.setUseWrapMode(true);
    this._editor.setFontSize(18);

    // Add the beautify extension
    var beautify = ace.require("ace/ext/beautify");
    beautify.beautify(this._editor.session);
    var langTools = ace.require("ace/ext/language_tools");

    // Add the keywords to the autocompletion
    var myCompleter = {
      getCompletions: function (editor: any, session: any, pos: any, prefix: any, callback: any) {
        var wordList = keywords;
        callback(null, wordList.map(function (word) {
          return {
            caption: word,
            value: word
          };
        }));
      }
    };
    langTools.addCompleter(myCompleter);

  }

  // Get the value of the editor
  public getValue() {
    return this._editor.getValue();
  }

  // Set the value of the editor
  public setValue(value: string) {
    this._editor.setValue(value);
    this._editor.clearSelection();
  }

  // Clear the selection
  public clearSelection() {
    this._editor.clearSelection();
  }

  // Insert a value at the current position
  public insert(value: string) {
    this._editor.insert(value);
  }

  // Go to a specific line
  public goToLine(line: number) {
    this._editor.gotoLine(line);
  }

  // Focus and select the current line
  public focus() {
    this._editor.focus();
    this._editor.selection.selectLine();
  }

  // Set the theme
  public setTheme(theme: string) {
    this._editor.setTheme(theme);
  }

  // Get the theme
  public getTheme() {
    return this._editor.getTheme();
  }

  // Set read only
  public setReadOnly(value: boolean) {
    this._editor.setReadOnly(value);
    // Cursor not allowed
    this._editor.container.style.cursor = "not-allowed";
    this._editor.renderer.setStyle("disabled", true)
    this._editor.blur()
  }

  // Highlight a text pattern
  public highlightBlock(text: string) {
    let range = this._editor.find(text, {
      wrap: true,
      caseSensitive: false,
      wholeWord: true,
      regExp: false,
      preventScroll: false
    });
    this._editor.selection.setRange(range);
  }

  // Add the editor to the DOM
  public addToDOM(name: string) {
    console.log("Adding " + name + " to the DOM");
    let div = document.createElement("div");
    div.setAttribute("id", name);
    // Add class d-none to the div
    div.classList.add("d-none");
    div.classList.add("editor");
    // MODIF
    let editorContainer = document.getElementById("editor-card-body") as HTMLElement;
    editorContainer.appendChild(div);
  }

  // Hide the editor
  public hide() {
    console.log("Hiding " + this._name);
    let div = document.getElementById(this._name) as HTMLElement;
    if (div.classList.contains("d-none")) {
      return;
    }
    div.classList.add("d-none");
  }

  // Show the editor
  public show() {
    console.log("Showing " + this._name);
    let div = document.getElementById(this._name) as HTMLElement;
    if (!div.classList.contains("d-none")) {
      return;
    }
    div.classList.remove("d-none");
  }
}

