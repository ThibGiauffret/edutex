import GUI from "./gui";
import DB from "./db";
import Projects from "./projects";

// Get the params in the URL
const url = new URL(window.location.href);
const params = url.searchParams;

// Get the values stored in the localStorage
if (localStorage.getItem("engine") === null) {
  localStorage.setItem("engine", "pdflatex");
}
let engine = localStorage.getItem("engine");
let theme = localStorage.getItem("theme");
let autosave: number = 120;
let autosaveInterval = localStorage.getItem("autosaveInterval") as unknown as number;
if ( autosaveInterval !== null) {
  autosave = autosaveInterval/1000 
}

// Build the page properties
const pageProperties = {
  workspace: params.get("workspace"),
  assets : params.get("assets"),
  from : params.get("from"),
  mode : params.get("mode"),
  fresh : params.get("fresh"),
  engine : engine,
  theme : theme,
  autosave : autosave.toString()
};

// Create the objects
export const db = new DB();
export const projects = new Projects();
export const gui = new GUI(pageProperties);

// Initialize the GUI
gui.init();