# EduTex

EduTex is a web application to create and share small LaTeX documents. It is based on [SwiftLaTeX](https://www.swiftlatex.com/) to compile LaTeX code into PDF files (thanks to WebAssembly technology).

## Disclaimer

- This project is still in development and is not ready for production. Many features are missing and bugs are to be expected.
- My code is far from perfect and I'm still learning. You may find some aberrations (about performance and security), feel free to report them.
- The app interface is in French and is not translated yet.

## Installation

- Clone the repository
- Install dependencies with `npm install`
- Test the app with `npm run test`
- Build the app with `npm run build`
- The app is now ready to be deployed on a web server

## License

This project is licensed under the GNU GPL v3 License - see the [LICENSE](LICENSE) file for details.
